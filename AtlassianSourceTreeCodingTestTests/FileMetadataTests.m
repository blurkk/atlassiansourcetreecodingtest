//
//  FileMetadataTests.m
//  FileMetadataTests
//
//  Created by blurk on 21/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "FileMetadata.h"

@interface FileMetadataTests : XCTestCase

@end

@implementation FileMetadataTests
{
    // XCTest doesn't seem to like properties.
    NSString *path;
    NSString *name;
    NSString *kind;
    NSUInteger size;
    NSURL *url;
    FileMetadata *fm;
}

- (void)setUp
{
    [super setUp];
    /// @todo: I know this is bogus and will prevvent anyone else from testing. This needs to be more generic.
    path = @"/Users/blurk/.profile";
    kind = @"test kind";
    size = 42;
    url = [NSURL fileURLWithPath:@"/Users/blurk/.profile"];
    name = [url lastPathComponent];
    fm = [FileMetadata fileMetadataForUrl:url filename:name kind:kind size:@(size)];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// The URL is simplied supplied to FileMetadata.
// Check that it is reflected back unchanged.
- (void)testUrlIsTheSameAsSupplied
{
    XCTAssertEqualObjects(fm.url, url, @"");
}

// The name is simplied supplied to FileMetadata.
// Check that it is reflected back unchanged.
- (void)testNameIsTheSameAsSupplied
{
    XCTAssertEqualObjects(fm.name, name, @"");
}

// The file kind is simplied supplied to FileMetadata.
// Check that it is reflected back unchanged.
- (void)testKindIsTheSameAsSupplied
{
    XCTAssertEqualObjects(fm.kind, kind, @"");
}

// Check that the supplied size is formatted into a user readable string.
- (void)testSize
{
    NSString *expected = [NSString stringWithFormat:@"%lu bytes", size];
    XCTAssertEqualObjects(fm.size, expected, @"");
}

// We don't know the creation date of the test file, so just check that a
// string is returned.
-(void)testCreationDate
{
    XCTAssertNotNil(fm.creationDate, @"");
    XCTAssert([fm.creationDate isKindOfClass:[NSString class]],
              @"got class %@", [fm.creationDate class]);
}

// We don't know the modification date of the test file, so just check that a
// string is returned.
-(void)testModificationDate
{
    XCTAssertNotNil(fm.modificationDate, @"");
    XCTAssert([fm.modificationDate isKindOfClass:[NSString class]],
              @"got class %@", [fm.modificationDate class]);
}

// We don't know the icon of the test file, so just check that an image is returned.
-(void)testIcon
{
    XCTAssertNotNil(fm.icon, @"");
    XCTAssert([fm.icon isKindOfClass:[NSImage class]],
              @"got class %@", [fm.icon class]);
}

// The file's user permissions in Posix format.
-(void)testUserPermissions
{
    XCTAssertNotNil(fm.userPermissions, @"");
    XCTAssertEqualObjects(fm.userPermissions, @"blurk: rw-", @"");
}

// The file's group permissions in Posix format.
-(void)testGroupPermissions
{
    XCTAssertNotNil(fm.groupPermissions, @"");
    XCTAssertEqualObjects(fm.groupPermissions, @"staff: r--", @"");
}

// The file's world permissions in Posix format.
-(void)testWorldPermissions
{
    XCTAssertNotNil(fm.worldPermissions, @"");
    XCTAssertEqualObjects(fm.worldPermissions, @"everyone: r--", @"");
}

@end


#pragma mark -------------------------------------------------------------------

@interface FileMetadataForNotExistingFileTests : XCTestCase

@end

@implementation FileMetadataForNotExistingFileTests
{
    NSString *path;
    NSString *name;
    NSString *kind;
    NSUInteger size;
    NSURL *url;
    FileMetadata *fm;
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    path = @"/I/do/not/exist";
    kind = @"different test kind";
    size = 1024;
    url = [NSURL fileURLWithPath:@"/I/do/not/exist"];
    name = [url lastPathComponent];
    fm = [FileMetadata fileMetadataForUrl:url filename:name kind:kind size:@(size)];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// The URL is simplied supplied to FileMetadata.
// Check that it is reflected back unchanged.
- (void)testUrlIsTheSameAsSupplied
{
    XCTAssertEqualObjects(fm.url, url, @"");
}

// The name is simplied supplied to FileMetadata.
// Check that it is reflected back unchanged.
- (void)testNameIsTheSameAsSupplied
{
    XCTAssertEqualObjects(fm.name, name, @"");
}

// The file kind is simplied supplied to FileMetadata.
// Check that it is reflected back unchanged.
- (void)testKindIsTheSameAsSupplied
{
    XCTAssertEqualObjects(fm.kind, kind, @"");
}

// Check that the supplied size is formatted into a user readable string.
- (void)testSize
{
    // Yes, this involves too much knowledge of test input data:
    NSString *expected = [NSString stringWithFormat:@"%lu KB", size / 1024];
    XCTAssertEqualObjects(fm.size, expected, @"");
}

// Check that the creation date string indicates it is unknown.
-(void)testCreationDate
{
    XCTAssertNotNil(fm.creationDate, @"");
    XCTAssert([fm.creationDate isKindOfClass:[NSString class]],
              @"got class %@", [fm.creationDate class]);
    XCTAssertEqualObjects(fm.creationDate, @"<unknown>", @"");
}

// Check that the modification date string indicates it is unknown.
-(void)testModificationDate
{
    XCTAssertNotNil(fm.modificationDate, @"");
    XCTAssert([fm.modificationDate isKindOfClass:[NSString class]],
              @"got class %@", [fm.modificationDate class]);
    XCTAssertEqualObjects(fm.modificationDate, @"<unknown>", @"");
}

// OS X seems to happily provide an icon based on name alone, so just check that an image is returned.
-(void)testIcon
{
    XCTAssertNotNil(fm.icon, @"");
    XCTAssert([fm.icon isKindOfClass:[NSImage class]],
              @"got class %@", [fm.icon class]);
}

// The user is unknown and permissions blank.
-(void)testUserPermissions
{
    XCTAssertNotNil(fm.userPermissions, @"");
    XCTAssertEqualObjects(fm.userPermissions, @"<unknown>: ---", @"");
}

// The group is unknown and permissions blank.
-(void)testGroupPermissions
{
    XCTAssertNotNil(fm.groupPermissions, @"");
    XCTAssertEqualObjects(fm.groupPermissions, @"<unknown>: ---", @"");
}

// The permissions are blank.
-(void)testWorldPermissions
{
    XCTAssertNotNil(fm.worldPermissions, @"");
    XCTAssertEqualObjects(fm.worldPermissions, @"everyone: ---", @"");
}

@end

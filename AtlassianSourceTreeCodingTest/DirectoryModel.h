//
//  DirectoryModel.h
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 21/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DirectoryModel : NSObject

@property (strong) NSString *path;

@end

//
//  AppDelegate.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 21/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
}

 - (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

@end

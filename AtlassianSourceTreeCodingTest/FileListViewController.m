//
//  FileListViewController.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 22/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import <Quartz/Quartz.h>

#import "FileListViewController.h"

#import "DetailsWindowController.h"
#import "FileListDataSource.h"
#import "FileMetadata.h"

@interface FileListViewController ()

@property (weak) IBOutlet NSButton *openButton;
@property (weak) IBOutlet FileListDataSource *fileListDataSource;
@property (weak) IBOutlet NSTableView *fileListTable;

@property (strong) DetailsWindowController *detailsWindowController;

@end

@implementation FileListViewController

- (void)loadContents
{
    [self.fileListDataSource loadContents];
}

- (IBAction)recursionPressed:(NSButton *)sender
{
    [self.fileListDataSource loadContents];
}

- (IBAction)openPressed:(NSButton *)sender
{
    for (NSURL *url in [self.fileListDataSource fileUrlsForIndexSet:self.fileListTable.selectedRowIndexes])
    {
        [[NSWorkspace sharedWorkspace] openURL:url];
    }
}

- (IBAction)cancelPressed:(NSButton *)sender
{
    [[NSApplication sharedApplication] terminate:sender];
}

- (IBAction)searchChanged:(NSSearchField *)sender
{
    [self.fileListDataSource filterBy:sender.stringValue];
}

- (void)menuWillOpen:(NSMenu *)menu
{
    BOOL clickedOnValidRow = self.fileListTable.clickedRow != -1;
    for (NSMenuItem *item in menu.itemArray)
    {
        item.enabled = clickedOnValidRow;
    }
}

- (void)forRightClickedFilesDo:(void(^)(NSIndexSet *))block
{
    NSIndexSet *selectedIndexes = self.fileListTable.selectedRowIndexes;
    // If the clicked row is in selectedIndexes, then process all selectedIndexes. Otherwise, process only clickedRow.
    NSInteger clickedRow = self.fileListTable.clickedRow;
    if (clickedRow != -1 && ![selectedIndexes containsIndex:clickedRow])
    {
        selectedIndexes = [NSIndexSet indexSetWithIndex:clickedRow];
    }
    block(selectedIndexes);
}

- (IBAction)showInFinderSelected:(NSMenuItem *)sender
{
    [self forRightClickedFilesDo:^(NSIndexSet *indexes)
    {
        NSArray *urls = [self.fileListDataSource fileUrlsForIndexSet:indexes];
        [[NSWorkspace sharedWorkspace] activateFileViewerSelectingURLs:urls];
    }];
}

- (IBAction)copyPathSelected:(NSMenuItem *)sender
{
    [self forRightClickedFilesDo:^(NSIndexSet *indexes)
    {
        NSArray *paths = [self.fileListDataSource filePathsForIndexSet:indexes];
        NSPasteboard *pasteboard = [NSPasteboard generalPasteboard];
        [pasteboard clearContents];
        [pasteboard writeObjects:paths];
    }];
}

- (IBAction)detailsItemSelected:(NSMenuItem *)sender
{
    if (!self.detailsWindowController)
    {
        self.detailsWindowController = [DetailsWindowController new];
    }
    [self.detailsWindowController showWindow:nil];
    [self forRightClickedFilesDo:^(NSIndexSet *indexes)
    {
        NSArray *files = [self.fileListDataSource fileMetadataForIndexSet:indexes];
        [self.detailsWindowController displayDetailsForFiles:files];
    }];
}

@end

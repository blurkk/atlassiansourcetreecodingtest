//
//  DetailsWindowController.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 24/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import "DetailsWindowController.h"

#import "FileMetadata.h"

#import <Quartz/Quartz.h>

// See:
// http://openradar.appspot.com/12794367
// https://github.com/curtclifton/renamer/commit/026d6b060c4269f415b1a812ac17507e04494ba3
@interface RepairQuickLookPreviewView : QLPreviewView

@end

@implementation RepairQuickLookPreviewView

// This is a work-around for Radar 12794367:
- (void)updateConstraints;
{
    NSView *subview = self.subviews[0];

    [subview setTranslatesAutoresizingMaskIntoConstraints:NO];
    [super updateConstraints];

    NSString *horizontalHuggingConstraints = @"H:|-(0)-[view]-(0)-|";
    NSString *verticalHuggingConstraints = @"V:|-(0)-[view]-(0)-|";

    // Make subview fit to us:
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:horizontalHuggingConstraints options:0 metrics:nil views:@{@"view":subview}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalHuggingConstraints options:0 metrics:nil views:@{@"view":subview}]];
}

@end

@interface DetailsWindowController ()

@property (strong) NSDateFormatter *dateFormatter;
@property (strong) NSArray *fileDetailsList;
@property NSUInteger currentlyDisplayedIndex;

@property (weak) IBOutlet NSTextField *fileTypeLabel;
@property (weak) IBOutlet NSTextField *sizeLabel;
@property (weak) IBOutlet NSTextField *creationDateLabel;
@property (weak) IBOutlet NSTextField *modificationDateLabel;
@property (weak) IBOutlet NSTextField *userPermissionsLabel;
@property (weak) IBOutlet NSTextField *groupPermissionsLabel;
@property (weak) IBOutlet NSTextField *worldPermissionsLabel;
@property (weak) IBOutlet NSTextField *fullPathLabel;
@property (weak) IBOutlet NSImageView *fileIconImageView;
@property (weak) IBOutlet QLPreviewView *previewView;
@property (weak) IBOutlet NSSegmentedControl *navControl;

@end

@implementation DetailsWindowController

- (id)init
{
    return [super initWithWindowNibName:@"DetailsWindow"];
}

- (void)windowDidLoad
{
    [super windowDidLoad];

    self.dateFormatter = [NSDateFormatter new];
    self.dateFormatter.dateStyle = NSDateFormatterLongStyle;
    self.dateFormatter.timeStyle = NSDateFormatterLongStyle;
    self.dateFormatter.doesRelativeDateFormatting = YES;
}

- (void)displayDetailsForFiles:(NSArray *)files
{
    self.fileDetailsList = files;
    // If only one file is being displayed, we don't need the buttons that
    // navigate between the files.
    self.navControl.hidden = self.fileDetailsList.count < 2;
    self.currentlyDisplayedIndex = 0;
    self.previewView.shouldCloseWithWindow = NO;
    // Make robust against code changes that may send an empty array.
    if (self.fileDetailsList.count > 0)
    {
        [self displayDetailsForCurrentIndex];
    }
}

- (IBAction)navControlChanged:(NSSegmentedControl *)sender
{
    if (sender.selectedSegment == 0)
    {
        self.currentlyDisplayedIndex = (self.currentlyDisplayedIndex) == 0 ? self.fileDetailsList.count - 1
                                                                           :  self.currentlyDisplayedIndex - 1;
    }
    else
    {
        self.currentlyDisplayedIndex = (self.currentlyDisplayedIndex + 1) % self.fileDetailsList.count;
    }
    [self displayDetailsForCurrentIndex];
}

- (void)displayDetailsForCurrentIndex
{
    FileMetadata *currentFileInfo = self.fileDetailsList[self.currentlyDisplayedIndex];
    self.fileTypeLabel.stringValue = currentFileInfo.kind;
    self.sizeLabel.stringValue = currentFileInfo.size;
    self.creationDateLabel.stringValue = currentFileInfo.creationDate;
    self.modificationDateLabel.stringValue = currentFileInfo.modificationDate;
    self.userPermissionsLabel.stringValue = currentFileInfo.userPermissions;
    self.groupPermissionsLabel.stringValue = currentFileInfo.groupPermissions;
    self.worldPermissionsLabel.stringValue = currentFileInfo.worldPermissions;
    self.fullPathLabel.stringValue = currentFileInfo.path;
    self.fileIconImageView.image = currentFileInfo.icon;
    self.previewView.previewItem = currentFileInfo.url;
    self.window.title = currentFileInfo.name;
}

@end

//
//  FileListDataSource.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 22/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import "FileListDataSource.h"

#import "DirectoryModel.h"
#import "FileMetadata.h"

#include <libkern/OSAtomic.h>
#include <time.h>

@interface FileListDataSource ()

@property (weak) IBOutlet DirectoryModel *directoryModel;
@property (weak) IBOutlet NSArrayController *arrayController;

@property (weak) NSFileManager *fileManager;
@property (nonatomic, strong) dispatch_queue_t fileEnumerationQueue;
@property (nonatomic, strong) NSMutableArray *fileList;

@property BOOL isLoading;

@end

@implementation FileListDataSource
{
    int32_t _operationCount;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"AC.selectionIndexes: %@; AC.selection: %@; AC.selectedObjects: %@",
        self.arrayController.selectionIndexes, self.arrayController.selection, self.arrayController.selectedObjects];
}

- (void)loadContents
{
    if (!self.fileEnumerationQueue)
    {
        self.fileEnumerationQueue = dispatch_queue_create("com.blurk.astct.fileEnumerationQueue", NULL);
        // Also, take this opportunity to lazy initialise other properties we
        // only need for loading the contents.
        self.fileManager = [NSFileManager defaultManager];
    }
    self.fileList = [NSMutableArray new];
    self.isLoading = YES;
    OSAtomicIncrement32(&_operationCount);
    dispatch_async(self.fileEnumerationQueue,
    ^{
        [self enumerateFilesAtPath:self.directoryModel.path intoList:self.fileList operationCount:_operationCount];
    });
}

- (void)filterBy:(NSString *)filter
{
    if ([filter length] > 0)
    {
        NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"%K CONTAINS %@", @"name", filter];
        self.arrayController.filterPredicate = filterPredicate;
    }
    else
    {
        // Clear the search filter
        self.arrayController.filterPredicate = nil;
    }
}

- (void)enumerateFilesAtPath:(NSString *)path intoList:(NSMutableArray *)fileList operationCount:(int32_t)operationCount
{
    NSURL *dirUrl = [NSURL fileURLWithPath:path];
    NSDirectoryEnumerationOptions mask = self.isRecursive ? 0 : NSDirectoryEnumerationSkipsSubdirectoryDescendants;
    NSDirectoryEnumerator *enumerator = [self.fileManager enumeratorAtURL:dirUrl
                                               includingPropertiesForKeys:@[
                                                                             NSURLIsDirectoryKey,
                                                                             NSURLNameKey,
                                                                             NSURLLocalizedTypeDescriptionKey,
                                                                             NSURLFileResourceTypeKey,
                                                                             NSURLFileSizeKey
                                                                           ]
                                                                  options:mask
                                                             errorHandler:^BOOL(NSURL *url, NSError *error)
    {
        if (error)
        {
            NSLog(@"Error enumerating directory %@: %@", url, error);
            return NO;
        }

        return YES;
    }];

    NSUInteger numFiles = 0;
    NSMutableArray *batchingFileList = [NSMutableArray array];
    time_t lastBatchTime = time(NULL);
    for (NSURL *fileUrl in enumerator)
    {
        if (operationCount != _operationCount)
        {
            break;
        }
        NSNumber *isDirectory;
        [fileUrl getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:nil];

        if (![isDirectory boolValue])
        {
            NSString *fileName;
            [fileUrl getResourceValue:&fileName forKey:NSURLNameKey error:nil];
            NSString *typeName;
            [fileUrl getResourceValue:&typeName forKey:NSURLLocalizedTypeDescriptionKey error:nil];
            if (!typeName)
            {
                static NSDictionary *kResourceTypeStrings = nil;
                if (!kResourceTypeStrings)
                {
                    kResourceTypeStrings =
                    @{
                        NSURLFileResourceTypeNamedPipe:        @"Named pipe",
                        NSURLFileResourceTypeCharacterSpecial: @"Character special file",
                        NSURLFileResourceTypeDirectory:        @"Directory",
                        NSURLFileResourceTypeBlockSpecial:     @"Block special file",
                        NSURLFileResourceTypeRegular:          @"Regular file",
                        NSURLFileResourceTypeSymbolicLink:     @"Symbolic link",
                        NSURLFileResourceTypeSocket:           @"Socket",
                        NSURLFileResourceTypeUnknown:          @"Unknown"
                    };
                }
                [fileUrl getResourceValue:&typeName forKey:NSURLFileResourceTypeKey error:nil];
                typeName = kResourceTypeStrings[typeName];
            }
            NSNumber *size;
            [fileUrl getResourceValue:&size forKey:NSURLFileSizeKey error:nil];
            if (numFiles < 100)
            {
                // Return the first batch of files pretty much immediately.
                // Sending the file back to the main thread one-by-one is actually
                // less efficient (and so arguably slows down a large load), but
                // the perception of something happening is important.
                ++numFiles;
                dispatch_async(dispatch_get_main_queue(),
                ^{
                    [self addFileURL:fileUrl name:fileName type:typeName size:size intoList:fileList];
                });
            }
            else
            {
                // The more files there are, the longer the interval between batches up to a maximum
                [batchingFileList addObject:[FileMetadata fileMetadataForUrl:fileUrl
                                                                    filename:fileName
                                                                        kind:typeName
                                                                        size:size]];
                time_t now = time(NULL);
                if (difftime(now, lastBatchTime) > 1)
                {
                    dispatch_async(dispatch_get_main_queue(),
                    ^{
                        [self addBatch:batchingFileList toList:fileList];
                    });
                    batchingFileList = [NSMutableArray array];
                    lastBatchTime = time(NULL);
                }
            }
        }
    }
    dispatch_async(dispatch_get_main_queue(),
    ^{
        if (operationCount == _operationCount)
        {
            if (batchingFileList.count > 0)
            {
                [self addBatch:batchingFileList toList:fileList];
            }
            self.isLoading = NO;
        }
    });
}

- (void)addFileURL:(NSURL *)url name:(NSString *)name type:(NSString *)type size:(NSNumber *)size intoList:(NSMutableArray *)fileList
{
    // Only add the file if data source's current file list is the same as the
    // one the enumerator started with. If it isn't, this means the user has
    // started another search, so we should discard any remnants of the old search.
    if (fileList == self.fileList)
    {
        [self willChangeValueForKey:@"fileList"];
        [self.fileList addObject:[FileMetadata fileMetadataForUrl:url
                                                         filename:name
                                                             kind:type
                                                             size:size]];
        [self didChangeValueForKey:@"fileList"];
    }
}

- (void)addBatch:(NSMutableArray *)batchedFileList toList:(NSMutableArray *)fileList
{
    // Only add the files if data source's current file list is the same as the
    // one the enumerator started with. If it isn't, this means the user has
    // started another search, so we should discard any remnants of the old search.
    if (fileList == self.fileList)
    {
        [self willChangeValueForKey:@"fileList"];
        [self.fileList addObjectsFromArray:batchedFileList];
        [self didChangeValueForKey:@"fileList"];
    }
}

- (NSArray *)fileMetadataForIndexSet:(NSIndexSet *)selectedIndexes
{
    NSMutableArray *selectedFileList = [NSMutableArray array];
    [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop)
    {
        // The index set represents indexes in a potentially filtered view, so
        // take care to look up via the array controller.
        [selectedFileList addObject:self.arrayController.arrangedObjects[idx]];
    }];
    return [NSArray arrayWithArray:selectedFileList];
}

- (NSArray *)fileUrlsForIndexSet:(NSIndexSet *)selectedIndexes
{
    NSMutableArray *fileUrls = [NSMutableArray array];
    [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop)
    {
        // The index set represents indexes in a potentially filtered view, so
        // take care to look up via the array controller.
        FileMetadata *fm = self.arrayController.arrangedObjects[idx];
        [fileUrls addObject:fm.url];
    }];
    return [NSArray arrayWithArray:fileUrls];
}

- (NSArray *)filePathsForIndexSet:(NSIndexSet *)selectedIndexes
{
    NSMutableArray *filePaths = [NSMutableArray array];
    [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop)
    {
        // The index set represents indexes in a potentially filtered view, so
        // take care to look up via the array controller.
        FileMetadata *fm = self.arrayController.arrangedObjects[idx];
        [filePaths addObject:[fm.url path]];
    }];
    return [NSArray arrayWithArray:filePaths];
}

@end

//
//  FileMetadata.h
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 25/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import <Foundation/Foundation.h>

// Encapsulates various pieces of metadata associated with a file, for display
// in the UI. Therefore, most of the properties are expressed as strings.
// Properties not specified in the factory method are lazily constructed.
@interface FileMetadata : NSObject

// The file: URL locating the file.
// Always present.
@property (strong, nonatomic) NSURL *url;

// The directory containing the file.
@property (strong, nonatomic) NSString *path;

// The file name without path.
@property (strong, nonatomic) NSString *name;

// The descriptive type of the file, called "Kind" in OS X's user terminology.
@property (strong, nonatomic) NSString *kind;

// The file's size as a user-displayable string.
@property (strong, nonatomic) NSString *size;

// The file's creation date as a user-displayable string.
@property (strong, nonatomic) NSString *creationDate;

// The file's modification date as a user-displayable string.
@property (strong, nonatomic) NSString *modificationDate;

// The file's icon.
@property (strong, nonatomic) NSImage *icon;

// The file's user permissions in Posix format.
@property (strong, nonatomic) NSString *userPermissions;

// The file's group permissions in Posix format.
@property (strong, nonatomic) NSString *groupPermissions;

// The file's world permissions in Posix format.
@property (strong, nonatomic) NSString *worldPermissions;

+ (instancetype)fileMetadataForUrl:(NSURL *)url filename:(NSString *)name kind:(NSString *)kind size:(NSNumber *)size;

@end

//
//  main.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 21/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}

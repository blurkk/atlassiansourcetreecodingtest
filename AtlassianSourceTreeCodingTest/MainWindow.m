//
//  MainWindow.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 22/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import "MainWindow.h"

@implementation MainWindow

- (void)restoreStateWithCoder:(NSCoder *)coder
{
    [super restoreStateWithCoder:coder];
    // Restoring the window state appears to trash the desired first responder by
    // restoring whatever was the first responder when the application last exited.
    // Force the first responder to be the initial first responder we have configured
    // in Interface Builder.
    [self makeFirstResponder:self.initialFirstResponder];
}

@end


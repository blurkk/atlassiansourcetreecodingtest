//
//  FileListDataSource.h
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 22/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileListDataSource : NSObject

@property BOOL isRecursive;

- (void)loadContents;
- (void)filterBy:(NSString *)filter;
- (NSArray *)fileMetadataForIndexSet:(NSIndexSet *)selectedIndexes;
- (NSArray *)fileUrlsForIndexSet:(NSIndexSet *)selectedIndexes;
- (NSArray *)filePathsForIndexSet:(NSIndexSet *)selectedIndexes;

@end

//
//  DirectoryModel.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 21/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import "DirectoryModel.h"

@implementation DirectoryModel

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: %p> path: %@", [self class], self, self.path];
}

@end

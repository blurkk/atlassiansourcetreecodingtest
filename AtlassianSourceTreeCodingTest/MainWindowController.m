//
//  MainWindowController.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 22/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import "MainWindowController.h"
#import "FileListViewController.h"

@interface MainWindowController ()

@property (weak) IBOutlet NSView *fileListView;
@property (strong) IBOutlet FileListViewController *fileListViewController;

@end

@implementation MainWindowController

- (void)displayNextView
{
    self.window.contentView = self.fileListView;
    [self.fileListViewController loadContents];
}

@end

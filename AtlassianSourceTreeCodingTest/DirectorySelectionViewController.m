//
//  DirectorySelectionViewController.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 21/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import "DirectorySelectionViewController.h"
#import "DirectoryModel.h"
#import "MainWindowController.h"


#pragma mark -------------------------------------------------------------------

// An NSFormatter subclass that provides a formatter to attach to the directory
// text input field. This formatter is actually used as a live input validator,
// that tests whether the directory exists per keystroke.
@interface DirFormatter: NSFormatter

@property BOOL isValidDir;

@end


@implementation DirFormatter

- (NSString *)stringForObjectValue:(id)obj
{
    return [obj isKindOfClass:[NSString class]] ? obj : @"";
}

- (BOOL)getObjectValue:(out id *)obj forString:(NSString *)string errorDescription:(out NSString **)error
{
    *obj = [NSString stringWithString:string];
    return YES;
}

- (BOOL)isPartialStringValid:(NSString *)partialString newEditingString:(NSString **)newString errorDescription:(NSString **)error
{
    NSURL *url = [NSURL fileURLWithPath:partialString];
    // OS X only creates a file reference URL if the file actually exists, and this
    // appears to be very fast. Use it as a quick filter of paths to test.
    NSURL *refUrl = [url fileReferenceURL];
    if (refUrl)
    {
        BOOL isDir = NO;
        [[NSFileManager defaultManager] fileExistsAtPath:partialString isDirectory:&isDir];
        self.isValidDir = isDir;
    }
    else
    {
        self.isValidDir = NO;
    }
    // The strings the user types are all valid for the text input field,
    // we are just using the formatter as a way of getting at the partial text.
    return YES;
}

@end


#pragma mark -------------------------------------------------------------------

@interface DirectorySelectionViewController ()

@property (weak) IBOutlet NSTextField *directoryInputField;
@property (weak) IBOutlet NSButton *selectDirectoryButton;
@property (weak) IBOutlet NSButton *cancelButton;
@property (weak) IBOutlet NSButton *nextButton;
@property (weak) IBOutlet DirectoryModel *directoryModel;

@end

@implementation DirectorySelectionViewController

- (IBAction)selectDirectoryPressed:(NSButton *)sender
{
    NSOpenPanel *directorySelectionPanel = [NSOpenPanel openPanel];
    directorySelectionPanel.canChooseFiles = NO;
    directorySelectionPanel.canChooseDirectories = YES;
    [directorySelectionPanel beginWithCompletionHandler:^(NSInteger result)
    {
        if (NSFileHandlingPanelOKButton == result)
        {
            self.directoryModel.path = directorySelectionPanel.URL.path;
            // Anything selected from the directory selection NSOpenPanel must be a valid path
            // (excluding race conditions, but they can occur even with manual typing).
            self.nextButton.enabled = YES;
        }
    }];
}

- (IBAction)nextPressed:(NSButton *)sender
{
    MainWindowController *mwc = self.view.window.windowController;
    [mwc displayNextView];
}

- (IBAction)cancelPressed:(NSButton *)sender
{
    [[NSApplication sharedApplication] terminate:sender];
}

@end

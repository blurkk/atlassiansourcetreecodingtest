//
//  DetailsWindowController.h
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 24/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DetailsWindowController : NSWindowController

- (void)displayDetailsForFiles:(NSArray *)files;

@end

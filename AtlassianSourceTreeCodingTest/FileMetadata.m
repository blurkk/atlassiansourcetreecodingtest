//
//  FileMetadata.m
//  AtlassianSourceTreeCodingTest
//
//  Created by blurk on 25/03/2015.
//  Copyright (c) 2015 Bernie Maier. All rights reserved.
//

#import "FileMetadata.h"

#include <sys/stat.h>


static NSString * const kUnknown = @"<unknown>";

@interface FileMetadata ()

@property (strong) NSByteCountFormatter *sizeFormatter;
@property (strong) NSDateFormatter *dateFormatter;
@property (strong) NSDictionary *fileAttributes;

@end

@implementation FileMetadata

+ (instancetype)fileMetadataForUrl:(NSURL *)url filename:(NSString *)name kind:(NSString *)kind size:(NSNumber *)size
{
    return [[[self class] alloc] initWithUrl:url filename:name kind:kind size:size];
}

- (instancetype)initWithUrl:(NSURL *)url filename:(NSString *)name kind:(NSString *)kind size:(NSNumber *)size
{
    self = [super init];
    if (self)
    {
        self.url = url;
        self.name = name;
        self.kind = kind;
        self.sizeFormatter = [NSByteCountFormatter new];
        self.dateFormatter = [NSDateFormatter new];
        self.dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        self.dateFormatter.timeStyle = NSDateFormatterMediumStyle;
        self.dateFormatter.doesRelativeDateFormatting = YES;
        self.size = size ? [self.sizeFormatter stringForObjectValue:size] : @"n/a";
    }
    return self;
}


#pragma mark -------------------------------------------------------------------
#pragma mark Accessor methods for lazily evaluated properties

// Lazily create the path property value
- (NSString *)path
{
    if (!_path)
    {
        _path = [[self.url URLByDeletingLastPathComponent] path];
    }
    return _path;
}

// Lazily create the creationDate property value
- (NSString *)creationDate
{
    if (!_creationDate)
    {
        _creationDate = [self dateStringOrUnknownForDate:[self getFileAttribute:NSFileCreationDate]];
    }
    return _creationDate;
}

// Lazily create the modificationDate property value
- (NSString *)modificationDate
{
    if (!_modificationDate)
    {
        _modificationDate = [self dateStringOrUnknownForDate:[self getFileAttribute:NSFileModificationDate]];
    }
    return _modificationDate;
}

// Lazily create the icon property value
- (NSImage *)icon
{
    if (!_icon)
    {
        _icon = [[NSWorkspace sharedWorkspace] iconForFile:[self.url path]];
    }
    return _icon;
}

// Lazily create the userPermissions property value
- (NSString *)userPermissions
{
    if (!_userPermissions)
    {
        NSString *name = [self getFileAttribute:NSFileOwnerAccountName];
        _userPermissions = [self permissionStringForName:name rwxBits:@[ @(S_IRUSR), @(S_IWUSR), @(S_IXUSR)]];
    }
    return _userPermissions;
}

// Lazily create the groupPermissions property value
- (NSString *)groupPermissions
{
    if (!_groupPermissions)
    {
        NSString *name = [self getFileAttribute:NSFileGroupOwnerAccountName];
        _groupPermissions = [self permissionStringForName:name rwxBits:@[ @(S_IRGRP), @(S_IWGRP), @(S_IXGRP)]];
    }
    return _groupPermissions;
}

// Lazily create the worldPermissions property value
- (NSString *)worldPermissions
{
    if (!_worldPermissions)
    {
        _worldPermissions = [self permissionStringForName:@"everyone" rwxBits:@[ @(S_IROTH), @(S_IWOTH), @(S_IXOTH)]];
    }
    return _worldPermissions;
}

#pragma mark -------------------------------------------------------------------

- (id)getFileAttribute:(NSString *)attributeKey
{
    if (!self.fileAttributes)
    {
        NSError *error = nil;
        self.fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[self.url path] error:&error];
        if (error || !self.fileAttributes)
        {
            NSLog(@"Couldn't get attributes for file %@: %@", self.url, error);
            // Construct a mostly empty placeholder attributes dictionary.
            // Missing keys return nil, which is OK for some attributes.
            self.fileAttributes = @{ NSFileOwnerAccountName: kUnknown, NSFileGroupOwnerAccountName: kUnknown };
        }
    }
    return self.fileAttributes[attributeKey];
}

- (NSString *)dateStringOrUnknownForDate:(NSDate *)dateOrNil
{
    return dateOrNil ? [self.dateFormatter stringFromDate:dateOrNil] : kUnknown;
}

- (NSString *)permissionStringForName:(NSString *)name rwxBits:(NSArray *)rwxBits
{
    mode_t mode = [[self getFileAttribute:NSFilePosixPermissions] shortValue];
    return [NSString stringWithFormat:@"%@: %s%s%s", name,
                                                     (mode & [rwxBits[0] intValue]) ? "r" : "-",
                                                     (mode & [rwxBits[1] intValue]) ? "w" : "-",
                                                     (mode & [rwxBits[2] intValue]) ? "x" : "-"];
}

@end
